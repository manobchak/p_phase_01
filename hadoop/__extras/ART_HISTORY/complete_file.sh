#*************************************************
#file_complete sh
#*************************************************

#!/bin/bash -e

kinit -kt AA611073.keytab AA611073@IUSER.IROOT.ADIDOM.COM
successDir=$1
count=$2
tmp=_tmp
file_fmt=file_fmt
Total_count=`hadoop fs -cat $successDir/$file_fmt|awk 'NF'|wc -l`
echo $Total_count
echo $successDir
echo $successDir/$file_fmt
Remain_count=`expr $Total_count - $count`
#hadoop fs -cat $successDir/$file_fmt$tmp |awk 'NF'| cat >$successDir/$file_fmt$tmp
hadoop fs -cat $successDir/$file_fmt|awk 'NF'|tail -$Remain_count>$file_fmt$tmp 
hadoop fs  -put -f  $file_fmt$tmp $successDir/
hadoop fs -chmod 770  $successDir/$file_fmt$tmp

echo "Total_count=$Total_count"
echo "Remain_count=$Remain_count"

hadoop fs -cat  $successDir/$file_fmt$tmp

