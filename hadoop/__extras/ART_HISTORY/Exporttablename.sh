#*************************************************
#file_SF_hold_control_check.sh
#*************************************************

#!/bin/bash -e

kinit -kt AA611073.keytab AA611073@IUSER.IROOT.ADIDOM.COM

successDir=$1
successFileName=$2
file_fmt=file_fmt
tmp=_tmp

var=`hadoop fs -cat $successDir/file_fmt|awk 'NF'|wc -l`
hadoop fs -cp -f  $successDir/$file_fmt   $successDir/$file_fmt$tmp
hadoop fs -chmod 770  $successDir/$file_fmt$tmp
echo "successcount=$var"
echo "tmp_file_name=$file_fmt$tmp
"
