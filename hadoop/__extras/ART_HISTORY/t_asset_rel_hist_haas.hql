use ${tgt_queue};
set mapreduce.job.queuename=${yarn_queueName};
set mapreduce.map.child.log.level=error;
set mapreduce.reduce.child.log.level=error;
set spark.yarn.queue=${yarn_queueName};
set hive.merge.mapredfiles=true;

SELECT  * FROM  w_edw_asset_haas a  WHERE  a.batch_num = '${batchcount}'  limit 1;