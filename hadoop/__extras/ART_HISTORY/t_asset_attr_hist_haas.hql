use ${tgt_queue};
set mapreduce.job.queuename=${yarn_queueName};
set mapreduce.map.child.log.level=error;
set mapreduce.reduce.child.log.level=error;
set spark.yarn.queue=${yarn_queueName};
set hive.execution.engine=spark;

INSERT OVERWRITE TABLE t_h_edw_assetattr_stg2
SELECT
 ASSET_ATTRIBUTE_KEY    ,
   ASSET_KEY            ,
   SIEBEL_ROW_ID        ,
   ATTRIBUTE_ID         ,
   ATTRIBUTE_NAME       ,
   ATTRIBUTE_DATA_TYPE  ,
   ATTRIBUTE_VALUE      ,
   SOURCE_SYSTEM        ,
   ATTRIBUTE_CLASS      ,
   DELETED_FLG          ,
   FILTER_LOB           ,
   FILTER_BAC           
from h_edw_asset_attribute  
where pmod(conv(substr(md5(concat_ws(asset_attribute_key,asset_key)),1,10),16,10),1000)=pmod(unix_timestamp(current_date)/86400,1000)
and hist_end_dt = '3000-12-31 00:00:00' ; 