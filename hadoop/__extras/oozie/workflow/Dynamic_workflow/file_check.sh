#*************************************************
#file_check.sh
#*************************************************
#!/bin/bash -e
kinit -kt EDWB2BP1.keytab EDWB2BP1@IUSER.IROOT.ADIDOM.COM
echo " start"
DeltaFileDir=$1
HIST_FILE_CHECK=$2
HIST_FILE_COUNT=$((`echo "$HIST_FILE_CHECK" | grep -o '|' | wc -l` ))
TODAY_DT=`date +%Y-%m-%d`
#This method performs the file check

echo "grep : "
hadoop fs -ls $DeltaFileDir|grep $TODAY_DT|grep -E $HIST_FILE_CHECK

echo "---------------------"
echo " file received:"
hadoop fs -ls $DeltaFileDir|grep $TODAY_DT|grep -E $HIST_FILE_CHECK

doFileCheck(){
# remove exit 0 above
    hist_tables_cnt=`hadoop fs -ls $DeltaFileDir|grep $TODAY_DT|grep -E $HIST_FILE_CHECK| wc -l`
           echo " Waiting for $hist_tables_cnt = $HIST_FILE_COUNT files and DL_files -----> " $HIST_FILE_CHECK
           echo  "==========================================================="
           echo " Total history $ DL table success received ----> "  `hadoop fs -ls $DeltaFileDir|grep $TODAY_DT|grep -E $HIST_FILE_CHECK`
        if [ $hist_tables_cnt -eq $HIST_FILE_COUNT  ]; then
      echo "SuccessCtrlFileCount=true"
	  NOW=$(date +"%m-%d-%Y-%T")
echo "Start_time=$NOW"
          exit 0
        fi

}

counter=1 # counter initialization

##Call the method to check for files
doFileCheck
#while [ `date +%H` -ge 16 ]
while(true)
do
        #If file is not found sleep for 15 mins
        sleep 1m
        #counter=counter+1

        if [ `date +%H` -ge 16 ]; then
          echo "SuccessCtrlFileCount=false"
          exit 1
        fi

        #perform the check again
        doFileCheck
done
echo "SuccessCtrlFileCount=false"
