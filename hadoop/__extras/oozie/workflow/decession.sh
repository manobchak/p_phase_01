#*************************************************
#Decession.sh
#*************************************************
#!/bin/bash -e
kinit -kt EDWB2BP1.keytab EDWB2BP1@IUSER.IROOT.ADIDOM.COM
  

counter=$1

OIFS=$IFS
IFS='@'
table_names=$2
count=1
for x in $table_names
do
   eval  'table_'$count=$x
	count=$[count+1]
done
count=1
action_names=$3
for x in $action_names
do
      eval   'action_'$count=$x
	count=$[count+1]
done
count=1
path_names=$4
for x in $path_names
do
       eval  'path_'$count=$x
	count=$[count+1]
done
count=1
schema_names=$5
for x in $schema_names
do
       eval  'schema_'$count=$x
	count=$[count+1]
done

count=1
query_names=$6
for x in $query_names
do
      eval   'query_'$count=$x
	count=$[count+1]
done

count=1
password_file_names=$7
for x in $password_file_names
do
      eval   'password_file_'$count=$x
	count=$[count+1]
done

count=1
db_username=$8
for x in $db_username
do
      eval   'db_username_'$count=$x
	count=$[count+1]
done

count=1
password_alias=$9
for x in $db_username
do
      eval   'password_alias_'$count=$x
	count=$[count+1]
done

var_table="table_$counter"
var_action="action_$counter"
var_schema="schema_$counter"
var_path="path_$counter"
var_query="query_$counter"
var_password="password_file_$counter"
var_db_username="db_username_$counter"
var_password_alias="password_alias_$counter"

table_name=${!var_table}
action_type=${!var_action}
schema_name=${!var_schema}
valid_path=${!var_path}
query=${!var_query}
password_file=${!var_password}
username=${!var_db_username}
pass_allias=${!var_password_alias}

echo  'TABLE_OR_SCRIPT_NAME='$table_name
echo  'ACTION_TYPE_OR_ARG='$action_type
echo  'VALID_PATH_OR_ARG='$valid_path
echo  'SCHEMA_OR_ARG='$schema_name
echo  'QUERY_OR_ARG='$query
echo  'PASSWORD_FILE='$password_file
echo  'USER_NAME='$username
echo  'PASSWORD_ALLIAS='$pass_allias

echo  'COUNTER='$1
echo  'table_name'=$2
echo 'action_type'=$3
echo 'schema_name'=$4
echo 'valid_path'=$5
echo 'query'=$6

