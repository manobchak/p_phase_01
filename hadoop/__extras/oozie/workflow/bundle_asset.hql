use ${tgt_queue};
set mapreduce.job.queuename=${yarn_queueName};

INSERT overwrite table  HAAS_DL_UKB_BUNDLE_ASSET 
         SELECT DISTINCT a.ASSET_KEY AS BUNDLE_ASSET_KEY,
                         c.product_cd BUNDLE_PRODUCT_CD,
                         c.product_name,
                         c.product_class,
                         c.product_subclass,
                         c.PRODUCT_SUBCLASS_2,
                         c.CONTRACT_TERM_MONTHS,
                         c.SUB_PRODUCT_BB    ,
                         c.SUB_PRODUCT_LINE ,
                         c.SUB_PRODUCT_CALLS    ,
                         c.SUB_PRODUCT_IP_OPTION   ,
                         c.PRODUCTS_IN_BUNDLE  ,
                         c.RELATIVE_VALUE,
                         a.ASSET_NUMBER AS BUNDLE_ASSET_NUMBER,
                         a.BILLING_ACCOUNT_KEY,
                         a.UPDATED_BY_USER_KEY,
                         a.SOURCE_UPDATE_DATE,
                         a.SERVICE_NUMBER,
                         a.INSTALL_DT,
                         a.SOURCE_SYSTEM,
                         a.ASSET_CREATE_DT,
                         a.DELETED_FLG,
                         a.FILTER_LOB,
                         a.NI_FLG,
                         a.BT_OWN_USE_IND,
                         a.ASSET_QUANTITY,
                         a.CONTRACT_START_DT,
                         a.CONTRACT_END_DT,
                         a.LAST_CPH_ORDER_KEY,
                         FROM_UNIXTIME(UNIX_TIMESTAMP(CONCAT(CAST(DATE_SUB(NEXT_DAY(CAST((CURRENT_DATE) AS STRING),'FRI'),7) AS STRING),' 23:59:59'))) AS DATE_CONSIDERED,
                     (UNIX_TIMESTAMP(CONCAT(CAST(DATE_SUB(NEXT_DAY(CAST((CURRENT_DATE) AS STRING),'FRI'),7) AS STRING),' 23:59:59'))) AS TS_CONSIDERED
           FROM HAASAAP0082_06429.H_EDW_ASSET A join  
                (select * from HAASAAP0082_06429.H_EDW_ASSET_REL) B
              ON (A.ASSET_KEY = B.LINKED_ASSET_KEY AND LINKED_ASSET_REL_TYPE = 'InBundle' AND b.DELETED_FLG = 'N' AND a.ASSET_STATUS = 'Active')
              left join W_EDW_PRODUCT c 
              ON (a.PRODUCT_KEY = c.PRODUCT_KEY )
          WHERE                 
            (UNIX_TIMESTAMP(CONCAT(CAST(DATE_SUB(NEXT_DAY(CAST((CURRENT_DATE) AS STRING),'FRI'),7) AS STRING),' 23:59:59')))
              BETWEEN
              (UNIX_TIMESTAMP(SUBSTR(A.hist_start_dt,1,19))) AND  (UNIX_TIMESTAMP(SUBSTR(A.hist_end_dt,1,19)))
               and 
                  (UNIX_TIMESTAMP(CONCAT(CAST(DATE_SUB(NEXT_DAY(CAST((CURRENT_DATE) AS STRING),'FRI'),7) AS STRING),' 23:59:59')))
               BETWEEN
               (UNIX_TIMESTAMP(SUBSTR(b.hist_start_dt,1,19))) AND  (UNIX_TIMESTAMP(SUBSTR(b.hist_end_dt,1,19)))
           union all
              SELECT DISTINCT a.ASSET_KEY AS BUNDLE_ASSET_KEY,
                         c.product_cd BUNDLE_PRODUCT_CD,
                         c.product_name,
                         c.product_class,
                         c.product_subclass,
                         c.PRODUCT_SUBCLASS_2,
                         c.CONTRACT_TERM_MONTHS,
                         c.SUB_PRODUCT_BB    ,
                         c.SUB_PRODUCT_LINE ,
                         c.SUB_PRODUCT_CALLS    ,
                         c.SUB_PRODUCT_IP_OPTION   ,
                         c.PRODUCTS_IN_BUNDLE  ,
                         c.RELATIVE_VALUE,
                         a.ASSET_NUMBER AS BUNDLE_ASSET_NUMBER,
                         a.BILLING_ACCOUNT_KEY,
                         a.UPDATED_BY_USER_KEY,
                         a.SOURCE_UPDATE_DATE,
                         a.SERVICE_NUMBER,
                         a.INSTALL_DT,
                         a.SOURCE_SYSTEM,
                         a.ASSET_CREATE_DT,
                         a.DELETED_FLG,
                         a.FILTER_LOB,
                         a.NI_FLG,
                         a.BT_OWN_USE_IND,
                         a.ASSET_QUANTITY,
                         a.CONTRACT_START_DT,
                         a.CONTRACT_END_DT,
                         a.LAST_CPH_ORDER_KEY,
                         FROM_UNIXTIME(UNIX_TIMESTAMP(CONCAT(CAST(DATE_SUB(NEXT_DAY(CAST((CURRENT_DATE) AS STRING),'FRI'),7) AS STRING),' 23:59:59'))+86400) AS DATE_CONSIDERED,
                        (UNIX_TIMESTAMP(CONCAT(CAST(DATE_SUB(NEXT_DAY(CAST((CURRENT_DATE) AS STRING),'FRI'),7) AS STRING),' 23:59:59'))+86400) AS TS_CONSIDERED
           FROM HAASAAP0082_06429.H_EDW_ASSET A join  
                (select * from HAASAAP0082_06429.H_EDW_ASSET_REL) B
           ON (A.ASSET_KEY = B.LINKED_ASSET_KEY AND LINKED_ASSET_REL_TYPE = 'InBundle' AND b.DELETED_FLG = 'N' AND a.ASSET_STATUS = 'Active')
            left join W_EDW_PRODUCT c 
            ON (a.PRODUCT_KEY = c.PRODUCT_KEY )
            inner join missing_bacs_os_friday miss
            on (miss.asset_key=A.asset_key)
          WHERE                 
             (UNIX_TIMESTAMP(CONCAT(CAST(DATE_SUB(NEXT_DAY(CAST((CURRENT_DATE) AS STRING),'FRI'),7) AS STRING),' 23:59:59'))+86400)
               BETWEEN
               (UNIX_TIMESTAMP(SUBSTR(A.hist_start_dt,1,19))) AND  (UNIX_TIMESTAMP(SUBSTR(A.hist_end_dt,1,19)))
               and 
                (UNIX_TIMESTAMP(CONCAT(CAST(DATE_SUB(NEXT_DAY(CAST((CURRENT_DATE) AS STRING),'FRI'),7) AS STRING),' 23:59:59'))+86400)
              BETWEEN
              (UNIX_TIMESTAMP(SUBSTR(b.hist_start_dt,1,19))) AND  (UNIX_TIMESTAMP(SUBSTR(b.hist_end_dt,1,19)));



INSERT overwrite table HAAS_DL_UKB_BUNDLE_CHILD   
                SELECT a.BUNDLE_ASSET_KEY,
                a.BUNDLE_ASSET_NUMBER,
                b.ASSET_KEY,
                c.product_cd,
                c.product_name,
                c.product_class,
                c.product_subclass,
                c.PRODUCT_SUBCLASS_2,
                c.CONTRACT_TERM_MONTHS,
                b.PARENT_ASSET_KEY,
                b.ASSET_STATUS,
                b.CONTRACT_START_DT,
                b.CONTRACT_END_DT,
                b.SOURCE_UPDATE_DATE,
                a.DATE_CONSIDERED,
                a.TS_CONSIDERED
           FROM HAAS_DL_UKB_BUNDLE_ASSET a inner join 
                HAASAAP0082_06429.H_EDW_ASSET b
                on (a.bundle_asset_key = b.PARENT_ASSET_KEY and b.ASSET_STATUS = 'Active')
                left join W_EDW_PRODUCT c
                on (b.PRODUCT_KEY = c.PRODUCT_KEY)
          WHERE  
               a.TS_CONSIDERED
               BETWEEN
               (UNIX_TIMESTAMP(SUBSTR(b.hist_start_dt,1,19))) AND  (UNIX_TIMESTAMP(SUBSTR(b.hist_end_dt,1,19)));





INSERT overwrite table HAAS_DL_UKB_BUNDLE_ASSET_ATTR  
           SELECT 
                  a.BUNDLE_ASSET_KEY,
                  a.BUNDLE_ASSET_NUMBER,
                  a.ASSET_KEY,
                  a.product_cd,
                  a.product_name,
                  a.product_class,
                  a.product_subclass,
                  a.PARENT_ASSET_KEY,
                  a.ASSET_STATUS,
                  a.CONTRACT_START_DT,
                  a.CONTRACT_END_DT,
                  a.SOURCE_UPDATE_DATE,
                  MAX (
                     CASE
                        WHEN (UPPER(ATTRIBUTE_NAME) in ('OFFERNAME','OFFERID','DISCOUNTLEVEL','DISCOUNTTERM','DISCOUNTTERMUNIT') AND ATTRIBUTE_VALUE IS NOT NULL)THEN 'Y'
                        ELSE 'N'
                     END)
                     AS OFFER_FLG,
                  MAX (
                     CASE
                        WHEN UPPER (ATTRIBUTE_NAME) = 'OFFERNAME'
                        THEN
                           ATTRIBUTE_VALUE
                        ELSE
                           NULL
                     END)
                     AS OFFER_NAME,
                  MAX (
                     CASE
                        WHEN UPPER (ATTRIBUTE_NAME) = 'OFFERID'
                        THEN
                           ATTRIBUTE_VALUE
                        ELSE
                           NULL
                     END)
                     AS OFFER_ID,
                  MAX (
                     CASE
                        WHEN UPPER (ATTRIBUTE_NAME) = 'DISCOUNTLEVEL'
                        THEN
                           ATTRIBUTE_VALUE
                        ELSE
                           NULL
                     END)
                     AS DISCOUNT_LEVEL,
                  MAX (
                     CASE
                        WHEN UPPER (ATTRIBUTE_NAME) = 'DISCOUNTTERM'
                        THEN
                           ATTRIBUTE_VALUE
                        ELSE
                           NULL
                     END)
                     AS DISCOUNT_TERM,
                  MAX (
                     CASE
                        WHEN UPPER (ATTRIBUTE_NAME) = 'DISCOUNTTERMUNIT'
                        THEN
                           ATTRIBUTE_VALUE
                        ELSE
                           NULL
                     END)
                     AS DISCOUNT_TERM_UNIT
             FROM HAAS_DL_UKB_BUNDLE_CHILD a left join 
                  HAASAAP0082_06429.H_EDW_ASSET_ATTRIBUTE b
         on (a.ASSET_KEY = b.ASSET_KEY)
         where a.TS_CONSIDERED
               BETWEEN
               (UNIX_TIMESTAMP(SUBSTR(b.hist_start_dt,1,19))) AND  (UNIX_TIMESTAMP(SUBSTR(b.hist_end_dt,1,19)))
         GROUP BY a.BUNDLE_ASSET_KEY,
                  a.BUNDLE_ASSET_NUMBER,
                  a.ASSET_KEY,
                  a.product_cd,
                  a.product_name,
                  a.product_class,
                  a.product_subclass,
                  a.PARENT_ASSET_KEY,
                  a.ASSET_STATUS,
                  a.CONTRACT_START_DT,
                  a.CONTRACT_END_DT,
                  a.SOURCE_UPDATE_DATE;

insert overwrite table  HAAS_DL_UKB_BUNDLE_HIERARCHY  
select a.bundle_asset_key,a.bundle_asset_number,b.main_asset_key,c.asset_type,c.product_key,c.billing_account_key,prod.SUB_PRODUCT_BB,prod.SUB_PRODUCT_line,prod.SUB_PRODUCT_calls,prod.SUB_PRODUCT_ip_option, 'Bundle Component' AS HIERARCHY_LEVEL
from 
HAAS_DL_UKB_BUNDLE_ASSET a inner join 
(select * from HAASAAP0082_06429.h_edw_asset_rel) b
 on (a.bundle_asset_key=b.linked_asset_key and b.LINKED_ASSET_REL_TYPE='InBundle' and b.DELETED_FLG='N')
inner join HAASAAP0082_06429.h_edw_asset c 
on (b.main_asset_key=c.asset_key  )
left join w_edw_product prod
on (prod.product_key=c.product_key)
where 
a.TS_CONSIDERED
               BETWEEN
               (UNIX_TIMESTAMP(SUBSTR(b.hist_start_dt,1,19))) AND  (UNIX_TIMESTAMP(SUBSTR(b.hist_end_dt,1,19)))
and 
a.TS_CONSIDERED
               BETWEEN
               (UNIX_TIMESTAMP(SUBSTR(c.hist_start_dt,1,19))) AND  (UNIX_TIMESTAMP(SUBSTR(c.hist_end_dt,1,19)));