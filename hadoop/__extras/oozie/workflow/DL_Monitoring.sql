************** DB 1 **************

-- IL DB L_EDW* to IL DB W_EDW_*_HAAS
-- Complete by 11.30 AM IST/6.00 AM BST

SELECT *
FROM   OPS$EDWGS.X_EDW_UKBS_INCR_DATE
WHERE  SOURCE_SCHEMA = 'ALL'
AND    TABLE_NAME LIKE '%ASSET%'

-- Checkint INT_OWNER_BUS_IL.W_EDW_ASSET_CSS_SP_HAAS

SELECT /*+ PARALLEL(A 16) */ MAX(EDW_INSERT_DATE)
FROM INT_OWNER_BUS_IL.W_EDW_ASSET_CSS_SP_HAAS

************** HAAS **************

-- Check for sqoop import of 9 tables

oozie job -oozie http://tplhc01c001.iuser.iroot.adidom.com:11000/oozie -info <coordinatotr_id>

oozie job -oozie http://tplhc01c001.iuser.iroot.adidom.com:11000/oozie -info <workflow_id>

******** DB2 **********

-- Sqoop export details

SELECT 
  HAAS_TABLE_NAME, 
  MAX(LOAD_END_DATE) AS MAX_LOAD_END_DATE
FROM   OPS$EDWGS.HAAS_TO_DB_SQOOP_CTRL_TABLE
WHERE HAAS_TABLE_NAME IN (
  'HAAS_UKB_BB_PROMOTION_ASSET',
  'HAAS_UKB_BB_ACCESS_ASSET',
  'HAAS_UKB_BB_ASSET_ATTR',
  'HAAS_UKB_BB_DISCOUNT_ASSET_NEW',
  'HAAS_UKB_BB_LINKED_ASSET',
  'HAAS_UKB_BB_PROMOTION_ASSET',
  'HAAS_WEEKLY_LINES_RSS_1',
  'HAAS_WEEKLY_LINES_RSS_1_CH',
  'HAAS_WEEKLY_LINES_RSS_4A_DCNT',
  'W_EDW_WEEKLY_BDBND_IRS_1',
  'W_EDW_WEEKLY_BDBND_IRS_4D_DCNT',
  'W_EDW_WKLY_BTNET_HAAS_STG1',
  'W_EDW_WKLY_BTNET_HAAS_STG2',
  'W_EDW_WKLY_BTNET_HAAS_STG3',
  'W_EDW_WKLY_BTNET_HAAS_STG4',
  'W_EDW_WKLY_BTNET_HAAS_STG5',
  'W_WKLY_LINES_RSS_AUX_ACCESS',
  'W_WKLY_LINES_RSS_AUX_CHILD',
  'W_WKLY_LINES_RSS_AUX_CONT',
  'W_WKLY_LINES_RSS_AUX_CONT_P',
  'W_WKLY_LINES_RSS_AUX_DISC_S1',
  'W_WKLY_LINES_RSS_AUX_GCHILD',
  'W_WKLY_LINES_RSS_AUX_GGCHILD',
  'W_WKLY_LINES_RSS_AUX_PARENT',
  'W_WKLY_LINES_RSS_FEAT_ACCESS_1',
  'W_WKLY_LINES_RSS_FEAT_CHILD',
  'W_WKLY_LINES_RSS_FEAT_DISC_CH',
  'W_WKLY_LINES_RSS_FEAT_PARENT',
  'W_WKLY_LINES_RSS_ISDN_ACCESS',
  'W_WKLY_LINES_RSS_ISDN_ATTR',
  'W_WKLY_LINES_RSS_ISDN_ATTR_S1',
  'W_WKLY_LINES_RSS_ISDN_BEARER',
  'W_WKLY_LINES_RSS_ISDN_CHILD',
  'W_WKLY_LINES_RSS_ISDN_CONT',
  'W_WKLY_LINES_RSS_ISDN_CONT_PRE',
  'W_WKLY_LINES_RSS_ISDN_DISC_CH',
  'W_WKLY_LINES_RSS_ISDN_DISC_GC',
  'W_WKLY_LINES_RSS_ISDN_DISC_GGC',
  'W_WKLY_LINES_RSS_ISDN_DISC_S1',
  'W_WKLY_LINES_RSS_ISDN_DISC_S2',
  'W_WKLY_LINES_RSS_ISDN_GCHILD',
  'W_WKLY_LINES_RSS_ISDN_GGCHILD',
  'W_WKLY_LINES_RSS_ISDN_PARENT',
  'W_WKLY_RSS_PSTNSL_STG_P',
  'W_WKLY_RSS_PSTNSL_STG1',
  'W_WKLY_RSS_PSTNSL_STG2',
  'W_WKLY_RSS_PSTNSL_STG3',
  'W_WKLY_RSS_PSTNSL_STG4',
  'W_WKLY_RSS_PSTNSL_STG5',
  'HAAS_UKB_BB_ACCESS_ASSET',
  -- 'HAAS_UKB_BB_CHILD_ASSET_EXT', Not loading
  -- 'HAAS_UKB_BB_DISCOUNT_ASSET', Not loading
  'HAAS_UKB_BB_LINKED_ASSET'
)
GROUP BY HAAS_TABLE_NAME
ORDER BY MAX(LOAD_END_DATE) DESC;

-- Check if there are duplicate success files. The hql will fail in that case.
hadoop fs -ls -C /user/HAASAAP0082_06429/landing/staging/il_history/delta_success/ | cut -d '.' -f 1 | cut -d '/' -f 8 | uniq -c | sort -nr | awk '{if($1>1) print $2 " -> " $1}'

-- DL stage load. Total count should be 95 rows.

SELECT   /*+ parallel (A,8)*/
         *
FROM     LU_LOG_MESSAGES A
WHERE    (MSG_ATTRIBUTE_1 LIKE ('%PKG_W_WKLY_BROADBAND_STG%')
OR        MSG_ATTRIBUTE_1 LIKE ('%PKG_WKLY_LINES_RSS_CSS_STG%')
OR        MSG_ATTRIBUTE_1 LIKE ('%PKG_WKLY_LINES_RSS_FEATURE%')
OR        MSG_ATTRIBUTE_1 LIKE ('%PKG_WKLY_LINES_RSS_PSTN_AUX%')
OR        MSG_ATTRIBUTE_1 LIKE ('%PKG_WKLY_LINES_RSS_ISDN30%')
OR        MSG_ATTRIBUTE_1 LIKE ('%PKG_WKLY_RSS_PSTNSL%')
OR        MSG_ATTRIBUTE_1 LIKE ('%PKG_F_WKLY_BROADBAND_IRS_STG%')
OR        MSG_ATTRIBUTE_1 LIKE ('%PKG_BTNET_CSS_STAGE_LOAD%')
OR        MSG_ATTRIBUTE_1 LIKE ('%PKG_W_WKLY_BUNDLE_STG%'))
AND      TRUNC (LOG_DATE) = TRUNC (SYSDATE)
ORDER BY LOG_DATE DESC;

-- DL load. Total count 17 rows

SELECT /*+ parallel (A,8)*/ *
FROM LU_LOG_MESSAGES A
WHERE(
MSG_ATTRIBUTE_1 LIKE ('%EDW_LOAD_STAGE_BUSIL.PKG_DLY_W_PRODUCT_RSS_STG.PR_DLY_W_PRODUCT_RSS_STG1%')
OR MSG_ATTRIBUTE_1 LIKE ('%INT_OWNER_BUS_IL.PKG_F_DLY_EDW_PRODUCT_RSS.PR_F_DLY_EDW_PRODUCT_RSS%')
OR MSG_ATTRIBUTE_1 LIKE ('%INT_OWNER_BUS_IL.PKG_F_DLY_EDW_PRODUCT_RSS.PR_F_WKLY_PROD_DELTA_RSS%')
OR MSG_ATTRIBUTE_1 LIKE ('%DM_OWNER_BUS_DL.PKG_F_WKLY_BILLING_ACCOUNT_RSS.PR_F_WKLY_BILLING_ACCOUNT_RSS%')
OR MSG_ATTRIBUTE_1 LIKE ('%DM_OWNER_BUS_DL.PKG_F_WKLY_BILLING_ACCOUNT_RSS.PR_F_WKLY_BAC_RSS_DELTA%')
OR MSG_ATTRIBUTE_1 LIKE ('%EDW_LOAD_STAGE_BUSIL.PR_MISSING_MIG_BAC%')
)
AND      TRUNC (LOG_DATE) = TRUNC (SYSDATE)
ORDER BY LOG_DATE DESC