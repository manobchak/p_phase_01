invalidate metadata miss_invoice_key_rental_flg

show create table miss_invoice_key_rental_flg

select * from miss_invoice_key_rental_flg

create external table haasaea0674_r0082.miss_invoice_key_rental_flg 
(
  invoice_id string comment 'from deserializer',    
  rental_flg string comment 'from deserializer',
)
with 
  serdeproperties(
    'serialization.format' = '1')
  stored as avro 
  location 'hdfs://nameservice1/user/haasaea0674_r0082/landing/staging/art_test/table_data/miss_invoice_key_rental_flg' 
  tblproperties(
    'avro.schema.url' = 'hdfs://nameservice1/user/haasaea0674_r0082/landing/staging/art_test/avro_files/miss_invoice_key_rental_flg.avsc',
    'numfiles' = '0', 
    'column_stats_accurate' = 'false', 
    'transient_lastddltime' = '1554230365', 
    'numrows' = '-1', 
    'totalsize' = '0', 
    'rawdatasize' = '-1')
    
describe formatted miss_invoice_key_rental_flg

create table cp1
as
select *
from miss_invoice_key_rental_flg

select * from cp1

describe formatted cp1

-- impala parquet
create table parquet_table (x int, y string) stored as parquet;

select * from parquet_table

-- hive parquet (try from hive)
CREATE EXTERNAL TABLE parquet_table (x INT, y STRING)
  ROW FORMAT SERDE 'parquet.hive.serde.ParquetHiveSerDe'
  STORED AS 
    INPUTFORMAT "parquet.hive.DeprecatedParquetInputFormat"
    OUTPUTFORMAT "parquet.hive.DeprecatedParquetOutputFormat"
    LOCATION 'hdfs://nameservice1/user/HAASAEA0674_R0082/landing/staging/art_test/table_data/parquet_table';
    
invalidate metadata parquet_table;

select * from parquet_table;

set parquet.compression=GZIP;

INSERT INTO parquet_table SELECT 2, invoice_id FROM miss_invoice_key_rental_flg;

describe formatted parquet_table;

--

select a.x, a.y, b.rental_flg
from parquet_table a, miss_invoice_key_rental_flg b
where a.y = b.invoice_id

select * from 