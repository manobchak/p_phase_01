drop table HAASAEA0674_R0082.miss_invoice_key_rental_flg

CREATE EXTERNAL TABLE HAASAEA0674_R0082.miss_invoice_key_rental_flg
  ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.avro.AvroSerDe'
  STORED as INPUTFORMAT 'org.apache.hadoop.hive.ql.io.avro.AvroContainerInputFormat'
  OUTPUTFORMAT 'org.apache.hadoop.hive.ql.io.avro.AvroContainerOutputFormat'
  LOCATION 'hdfs://nameservice1/user/HAASAEA0674_R0082/landing/staging/art_test/table_data/miss_invoice_key_rental_flg'
  TBLPROPERTIES ('avro.schema.url' = 'hdfs://nameservice1/user/HAASAEA0674_R0082/landing/staging/art_test/avro_files/miss_invoice_key_rental_flg.avsc');