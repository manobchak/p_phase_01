# replace the latin characters using iconv
cat fifa19_data_barcelona.csv | iconv -f latin1 -t ascii//TRANSLIT//IGNORE > fifa19_data_barcelona_load.csv
cat fifa19_data_bayern.csv | iconv -f latin1 -t ascii//TRANSLIT//IGNORE > fifa19_data_bayern_load.csv
cat fifa19_data_manu.csv | iconv -f latin1 -t ascii//TRANSLIT//IGNORE > fifa19_data_manu_load.csv

# create the directory and place the files
hdfs dfs -mkdir /user/HAASAEA0674_R0082/landing/staging/art_test/partition/
# put the files. replace if present.
hdfs dfs -put -f /home/mc801461/datafiles/fifa19_data*_load.csv /user/HAASAEA0674_R0082/landing/staging/art_test/partition/
hdfs dfs -ls /user/HAASAEA0674_R0082/landing/staging/art_test/partition/
hdfs dfs -chmod 777 /user/HAASAEA0674_R0082/landing/staging/art_test/partition/

# connect to hive
beeline --showHeader=false --outputformat=csv2 --hiveconf mapreduce.job.queuename=NONP.HAASAEA0674_R0082 -u 'jdbc:hive2://tplhc01c002:10000/'"HAASAEA0674_R0082"';principal=hive/tplhc01c002.iuser.iroot.adidom.com@IUSER.IROOT.ADIDOM.COM'

# partition external table
hdfs dfs -mkdir /user/HAASAEA0674_R0082/landing/staging/art_test/partition/fifa_19_data_ext/
hdfs dfs -chmod 777 /user/HAASAEA0674_R0082/landing/staging/art_test/partition/fifa_19_data_ext/

hdfs dfs -put -f /home/mc801461/datafiles/fifa19_data_manu_load.csv "/user/HAASAEA0674_R0082/landing/staging/art_test/partition/fifa_19_data_ext/club=Manchester United"
hdfs dfs -ls /user/HAASAEA0674_R0082/landing/staging/art_test/partition/fifa_19_data_ext/

# 
hdfs dfs -mkdir /user/HAASAEA0674_R0082/landing/staging/art_test/partition/test/