-- static partitioning in Hive Managed Table

drop table HAASAEA0674_R0082.FIFA_19_DATA_INT;

create table HAASAEA0674_R0082.FIFA_19_DATA_INT (
  ID int,
  NAME string,
  AGE tinyint,
  NATIONALITY string,
  OVERALL tinyint,
  POTENTIAL tinyint,
  CLUB_COL string,
  POSITION string,
  JOINED string,
  RELEASE_CLAUSE string
)
partitioned by (CLUB string)
row format delimited fields terminated BY ',' lines terminated BY '\n' 
tblproperties("skip.header.line.count"="1");

load data inpath 'hdfs://nameservice1/user/HAASAEA0674_R0082/landing/staging/art_test/partition/fifa19_data_barcelona_load.csv' overwrite into table HAASAEA0674_R0082.FIFA_19_DATA_INT PARTITION (CLUB='Barcelona');

load data inpath 'hdfs://nameservice1/user/HAASAEA0674_R0082/landing/staging/art_test/partition/fifa19_data_bayern_load.csv' overwrite into table HAASAEA0674_R0082.FIFA_19_DATA_INT PARTITION (CLUB='Bayern Munich');

show partitions HAASAEA0674_R0082.FIFA_19_DATA_INT;

select * from HAASAEA0674_R0082.FIFA_19_DATA_INT where CLUB = 'Barcelona';

--
