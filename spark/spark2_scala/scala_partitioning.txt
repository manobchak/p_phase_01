// https://stackoverflow.com/questions/45704156/what-is-the-difference-between-spark-sql-shuffle-partitions-and-spark-default-pa

spark2-shell --master yarn --deploy-mode client --num-executors 6 --executor-cores 2 --executor-memory 1g --conf spark.executor.memoryOverhead=500M – conf spark.dynamicAllocation.enabled=false

spark.conf.set("spark.sql.shuffle.partitions", "100");
spark.conf.set("spark.default.parallelism", "8");
sc.defaultParallelism
// executor details
sc.getExecutorMemoryStatus

val configMap:Map[String, String] = spark.conf.getAll;
// configMap("spark.sql.shuffle.partitions");
// configMap("spark.default.parallelism");

import org.apache.spark.storage.StorageLevel._;
val df_zips = spark.read.json("/user/stringmad/json_data").repartition($"STATE").withColumnRenamed("_id", "zip").persist(MEMORY_AND_DISK);
df_zips.rdd.getNumPartitions;

// row_number()
import org.apache.spark.sql.expressions.Window
df_zips.withColumn("row_num_state_pop", row_number().over(Window.partitionBy("STATE").orderBy($"pop".desc))).filter($"row_num_state_pop" === 1).count()

// repartition by a column. total partitions = total distinct values in the column
val v_state_count = df_zips.select($"STATE").distinct.count().toInt;
val df_zips_repart = df_zips.repartition(numPartitions = v_state_count, $"STATE")
df_zips.repartitionByRange(numPartitions = v_state_count, $"STATE").explain(true)





// Bucketing in Spark SQL
// get bucketing settings
spark.conf.set("spark.sql.sources.bucketing.enabled", "true");
val configMap:Map[String, String] = spark.conf.getAll;
configMap("spark.sql.sources.bucketing.enabled");

println(spark.version);
println(spark.sessionState.conf.bucketingEnabled);
