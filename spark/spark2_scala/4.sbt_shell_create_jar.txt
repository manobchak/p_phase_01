## Link: https://www.youtube.com/watch?v=o5KCtM83lQY

## In Unix, create the project folder.
mkdir projSbtScala
cd projSbtScala
mkdir -p src/main/scala

################# Enter the sbt shell
sbt

## Check the Dependencies
libraryDependencies

## set the dependencies, save the session and reload the session
## you can also set the dependencies from the build.sbt file
set scalaVersion := "2.11.8"
session save
reload

## spark-core_2.11 (2.11 is the scala version)
set libraryDependencies += "org.apache.spark" % "spark-core_2.11" % "2.1.1"
session save
reload

## %% asks sbt to add the current scala version to the artifact
## ex: libraryDependencies += "org.apache.spark" %% "spark-core" % "2.1.0"

## spark-sql also required from Spark 2
set libraryDependencies += "org.apache.spark" % "spark-sql_2.11" % "2.1.1"
session save
reload

## Check the Dependencies
libraryDependencies

exit

## the build.sbt file should contain. check the scala version.
## cat /home/manavchak1039/projSbtScala/build.sbt
scalaVersion := "2.11.8"
libraryDependencies += "org.apache.spark" % "spark-core_2.11" % "2.1.1"
libraryDependencies += "org.apache.spark" % "spark-sql_2.11" % "2.1.1"


################# Create the code file in src/main/scala to be compiled and create the jar file

## go to the scala folder
cd src/main/scala

## creat the .scala file with your code
cat Sample.scala
import org.apache.spark.sql.SparkSession;
object Sample{
    def main(args:Array[String]){
        val spark = SparkSession.builder().appName("test").master("local").getOrCreate();
        val rdd1 = spark.range(1,50);
        rdd1.collect().foreach(println);
        spark.stop();
    }
}

################# Enter the sbt shell

## Check the Dependencies
libraryDependencies

## Compile the program (Downloading takes time)
## It looks for the scala and spark libraries in the local sbt repository
## If not present locally, it downloads it from the maven repo.
compile

## You can test the code by issuing the command run
run

## To create the jar issue package
package

exit

################# In the UNIX box check the folder inside target/scala-2.11/*.jar
/home/manavchak1039/projSbtScala/target/scala-2.11

################# Run the prog using spark-submit
## Sample is the object containing the main class
spark-submit \
--class Sample \
/home/manavchak1039/projSbtScala/target/scala-2.11/projsbtscala_2.11-0.1.0-SNAPSHOT.jar