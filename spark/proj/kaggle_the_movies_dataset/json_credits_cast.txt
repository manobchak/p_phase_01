// create the dataframe
val file_location = "/user/stringmad/kaggle_movie_data/credits.csv"

:paste
val dfCredits = spark.read
.option("header", "true")
.option("delimiter", ",")
.option("inferSchema", "true")
.option("wholeFile", true)
.option("multiline",true)
.option("quote", "\"")
.option("escape", "\"")
//.option("encoding", "latin1")
.csv(file_location)
.toDF("cast", "crew", "movie_id")
.repartition(100, $"movie_id");

// split: split the records by "},"
// explode: convert the split rows into columns
// regexp_replace: replace the None values with string 'None' since from_json is not able to handle None
// replace the opening and closing square brackets
// replace the \xa0 characters
import org.apache.spark.sql.functions.{explode,split,regexp_replace,trim};

val regexp_replace_pattern="(\\[)|(])|(\\\\xa0)|(\\\\x92s)|(\\\\xad)";

val dfCreditsExplodeCast = dfCredits
.withColumn("split_cast_explode", explode(split($"cast", "},")))
.withColumn("split_cast_replace_none", regexp_replace($"split_cast_explode", ": None", ": 'None'"))
.withColumn("split_cast_replace_pattern", regexp_replace($"split_cast_replace_none", regexp_replace_pattern, ""))
.distinct
.select($"movie_id", trim($"split_cast_replace_pattern").alias("split_cast"));

// Fix the missing "}" in the end
import org.apache.spark.sql.functions.{lit,expr,concat,substring,when};

val dfCreditsExplodeCastFix = dfCreditsExplodeCast
.withColumn("substr_last", 
            when(
              expr("substring(split_cast, -1, 1)") === "}",
              $"split_cast"
            )
            .otherwise(concat($"split_cast", lit("}"))))
.distinct
.select($"movie_id", trim($"substr_last").alias("cast_explode"));

// Create the schema for the json
import org.apache.spark.sql.types.{StructType,StructField,StringType};

val cast_schema = StructType(
  List(
    StructField("cast_id", StringType, false),
    StructField("character", StringType, false),
    StructField("credit_id", StringType, false),
    StructField("gender", StringType, false),
    StructField("id", StringType, false),
    StructField("name", StringType, false),
    StructField("order", StringType, false),
    StructField("profile_path", StringType, false)
    )
  );
  
// create a df with the json format
import org.apache.spark.sql.functions.{from_json};

val dfCreditsExplodeCastJson = dfCreditsExplodeCastFix
.withColumn("cast_cols", from_json($"cast_explode", cast_schema))
.distinct
.select($"movie_id", $"cast_cols");

// Create the final df
val dfCreditsCastFinal = dfCreditsExplodeCastJson
.select(
  $"movie_id", 
  $"cast_cols.cast_id".alias("movie_cast_id"), 
  $"cast_cols.character".alias("cast_character"),
  $"cast_cols.credit_id".alias("cast_credit_id"),
  $"cast_cols.gender".alias("cast_person_gender_id"),
  $"cast_cols.id".alias("cast_person_id"),
  $"cast_cols.name".alias("cast_person_name"),
  $"cast_cols.order".alias("cast_credit_order"),
  $"cast_cols.profile_path".alias("cast_person_profile_path")
)
.distinct;