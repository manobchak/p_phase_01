credits.csv

hive_movie_cast
---------------
cast_credit_id : PK
movie_id : CK
cast_id : CK
cast_character
cast_credit_order
cast_person_id

hive_movie_crew
---------------
crew_credit_id : PK
movie_id : CK
crew_department
crew_job
crew_person_id

hive_movie_person
-----------------
person_id : PK
person_gender_id
person_name
person_profile_path

-------------------
-------------------

data sanity
-----------
1. record count check