// Databricks notebook source
// MAGIC %md # **LEVIS Data Engineer Coding Challenge**

// COMMAND ----------

// MAGIC %md 
// MAGIC ## Instructions
// MAGIC 
// MAGIC  1. Clone this notebook to your home folder.
// MAGIC  1. Solve as many of the problems below as you can within the alloted time frame. Some of the challenges are more advanced than others and are expected to take more time to solve.
// MAGIC  1. Unless otherwise instructed, you can use any programming languages (Python, SQL, Scala, etc.).
// MAGIC  1. You can create as many notebooks as you would like to answer the challenges.
// MAGIC  1. Notebooks should be presentable and should be able to execute succesfully with `Run All`.
// MAGIC  1. Once completed, publish your notebook: 
// MAGIC    * Choose the `Publish` item on the `File` menu
// MAGIC  1. Copy the URL(s) of the published notebooks and email them back to your LEVIS contact.

// COMMAND ----------

// MAGIC %md
// MAGIC ## Tips
// MAGIC - The Databricks Guide (in the top of the Workspace) provides examples for how to use Databricks. You may want to start by reading through it.

// COMMAND ----------

// MAGIC %md ### Using SQL in your cells
// MAGIC 
// MAGIC You can change to native SQL mode in your cells using the `%sql` prefix, demonstrated in the example below. Note that these include visualizations by default.

// COMMAND ----------

// MAGIC %sql
// MAGIC show tables

// COMMAND ----------

// MAGIC %md ### Creating Visualizations from non-SQL Cells
// MAGIC 
// MAGIC When you needs to create a visualization from a cell where you are not writing native SQL, use the `display` function, as demonstrated below.

// COMMAND ----------

// MAGIC %scala
// MAGIC val same_query_as_above = sqlContext.sql("show tables")

// COMMAND ----------

// MAGIC %scala
// MAGIC display(same_query_as_above)

// COMMAND ----------

// MAGIC %md ## Challenges
// MAGIC ---
// MAGIC 
// MAGIC ### TPC-H Dataset
// MAGIC You're provided with a TPCH data set. The data is located in `/databricks-datasets/tpch/data-001/`. You can see the directory structure below:

// COMMAND ----------

// MAGIC %scala
// MAGIC display(dbutils.fs.ls("/databricks-datasets/tpch/data-001/"))

// COMMAND ----------

// MAGIC %md As you can see above, this dataset consists of 8 different folders with different datasets. The schema of each dataset is demonstrated below: 

// COMMAND ----------

// MAGIC %md ![test](https://www.dropbox.com/s/kqqc36r1hfg8yqe/image_thumb2.png?dl=1)

// COMMAND ----------

// MAGIC %md You can take a quick look at each dataset by running the following Spark commmand. Feel free to explore and get familiar with this dataset

// COMMAND ----------

// MAGIC %scala
// MAGIC sc.textFile("/databricks-datasets/tpch/data-001/supplier/").take(1)

// COMMAND ----------

// MAGIC %md #### **Question #1**: Joins in Core Spark
// MAGIC Pick any two datasets and join them using Spark's API. Feel free to pick any two datasets. For example: `PART` and `PARTSUPP`. The goal of this exercise is not to derive anything meaningful out of this data but to demonstrate how to use Spark to join two datasets. For this problem you're **NOT allowed to use SparkSQL**. You can only use RDD API. You can use either Python or Scala to solve this problem.

// COMMAND ----------

// Join using RDD API

// Create a RDD for PART
val rddPart = sc.textFile("/databricks-datasets/tpch/data-001/part/")
rddPart.take(5)

// Create mapped RDD for PART to be used for join
// array(0) is the PARTKEY
// assuming we need NAME(array(1)) and MFGR(array(2)) as part of the join
val rddMapPart = rddPart.flatMap(item => item.split("\n")).map(line => line.split("\\|")).map(array => (array(0), Array(array(1), array(2))))
// rddMapPart.take(5)

// Create a RDD for PARTSUPP
val rddPartSupp = sc.textFile("/databricks-datasets/tpch/data-001/partsupp/")

// Create mapped RDD for PARTSUPP to be used for join
// array(0) is the PARTKEY
// assuming we need SUPPKEY(array(1)) as part of the join
val rddMapPartSupp = rddPartSupp.flatMap(item => item.split("\n")).map(line => line.split("\\|")).map(array => (array(0), array(1)))
// rddMapPartsUpp.take(5)

// COMMAND ----------

// Joining the PART and PARTSUPP on the keys (PARTKEY in both RDDs)
val rddJoin = rddMapPart.join(rddMapPartSupp)
// Checking the join output for PARTKEY = 1
rddJoin.filter(_._1 == "1").take(10)

// COMMAND ----------

// MAGIC %md #### **Question #2**: Joins With Spark SQL
// MAGIC Pick any two datasets and join them using SparkSQL API. Feel free to pick any two datasets. For example: PART and PARTSUPP. The goal of this exercise is not to derive anything meaningful out of this data but to demonstrate how to use Spark to join two datasets. For this problem you're **NOT allowed to use the RDD API**. You can only use SparkSQL API. You can use either Python or Scala to solve this problem. 

// COMMAND ----------

// Join using SQL API (Spark SQL)

// file path for the SUPPLIER dataset
val fileSupp = "/databricks-datasets/tpch/data-001/supplier/"

// import the data types for creating the schema
import org.apache.spark.sql.types._;

// create the schema for SUPPLIER data set
val dfSuppSchema = StructType(
  List(
    StructField("SUPPKEY", StringType, true),
    StructField("NAME", StringType, true),
    StructField("ADDRESS", StringType, true),
    StructField("NATIONKEY", IntegerType, true),
    StructField("PHONE", StringType, true),
    StructField("ACCTBAL", StringType, true),
    StructField("COMMENT", StringType, true)
    )
);

// create the SUPPLIER dataframe
val dfSupp = spark.read
.option("delimiter", "|")
.schema(dfSuppSchema)
.csv(fileSupp);

// dfSupp.show(5, false)

// COMMAND ----------

// file path for the NATION dataset
val fileNation = "/databricks-datasets/tpch/data-001/nation/"

// import the data types for creating the schema
import org.apache.spark.sql.types._;

// create the schema for NATION data set
val dfNationSchema = StructType(
  List(
    StructField("NATIONKEY", IntegerType, true),
    StructField("NAME", StringType, true),
    StructField("REGIONKEY", IntegerType, true),
    StructField("COMMENT", StringType, true)
    )
);

// create the NATION dataframe
val dfNation = spark.read
.option("delimiter", "|")
.schema(dfNationSchema)
.csv(fileNation);

// dfNation.show(5, false);

// COMMAND ----------

// create temporary tables to be used in the Spark SQL join
dfSupp.createOrReplaceTempView("tmpSupp");
dfNation.createOrReplaceTempView("tmpNation");

// Join the data sets on NATIONKEY. Fetch the Supplier information and the Nation's name.
spark.sql("""
select s.*, n.NAME as NATIONNAME
from tmpSupp s, tmpNation n
where s.NATIONKEY = n.NATIONKEY
""").show(5,false);

// COMMAND ----------

// MAGIC %md #### **Question #3**: Alternate Data Formats
// MAGIC The given dataset above is in raw text storage format. What other data storage format can you suggest to optimize the performance of our Spark workload if we were to frequently scan and read this dataset. Please come up with a code example and explain why you decide to go with this approach. Please note that there's no completely correct answer here. We're interested to hear your thoughts and see the implementation details.

// COMMAND ----------

// The performance can be optimized using a Columnar data storage format preferably ORC (with Zlib compression) or even Parquet (with Snappy compression).
// This would enhance the performance using with a local indexes, pushdown optimization and vectorization.
// As well as reduce the storage consumption with compression.

// Let us store the output of the previous join into a data frame.
val dfJoinOrc = spark.sql("""
select s.*, n.NAME as NATIONNAME
from tmpSupp s, tmpNation n
where s.NATIONKEY = n.NATIONKEY
""");

//dfJoinOrc.show(5, false);

// Setting the orc compression to Zlib since the default is Snappy most probably.
spark.conf.set("spark.sql.orc.compression.codec", "zlib");

// Writing it into the output location.
val outputOrcLoc = "/OrcOutput/";
dfJoinOrc.write.format("orc").mode("overwrite").save(outputOrcLoc);

// COMMAND ----------

// Reading the folder with the ORC files.
val dfOrc = spark.read.orc(outputOrcLoc);
dfOrc.show(5, false);

// COMMAND ----------

// MAGIC %md ### Baby Names Dataset
// MAGIC 
// MAGIC This dataset comes from a website referenced by [Data.gov](http://catalog.data.gov/dataset/baby-names-beginning-2007). It lists baby names used in the state of NY from 2007 to 2012.
// MAGIC 
// MAGIC The following cells run commands that copy this file to the cluster.

// COMMAND ----------

// MAGIC %fs rm dbfs:/tmp/rows.json

// COMMAND ----------

// MAGIC %scala
// MAGIC import java.net.URL
// MAGIC import java.io.File
// MAGIC import org.apache.commons.io.FileUtils
// MAGIC 
// MAGIC val tmpFile = new File("/tmp/rows.json")
// MAGIC FileUtils.copyURLToFile(new URL("https://health.data.ny.gov/api/views/jxy9-yhdk/rows.json?accessType=DOWNLOAD"), tmpFile)

// COMMAND ----------

// MAGIC %fs mv file:/tmp/rows.json dbfs:/tmp/rows.json

// COMMAND ----------

// MAGIC %fs head dbfs:/tmp/rows.json

// COMMAND ----------

// MAGIC %md #### **Question #1**: Spark SQL's Native JSON Support
// MAGIC Use Spark SQL's native JSON support to create a temp table you can use to query the data (you'll use the `registerTempTable` operation). Show a simple sample query.

// COMMAND ----------

// Json file path
val jsonInputFile = "/tmp/rows.json";
// Use the json format to read the file into a data frame.
val dfBabyNames = spark.read.option("multiline", true).json(jsonInputFile);

// Create a temp table on top of the data frame.
dfBabyNames.createOrReplaceTempView("tBabyNames");
// Use the Spark SQL API to query the temp table.
spark.sql("""
select *
from tBabyNames
""").show();

// COMMAND ----------

// MAGIC %md #### **Question #2**: Working with Nested Data
// MAGIC What does the nested schema of this dataset look like? How can you bring these nested fields up to the top level in a DataFrame?

// COMMAND ----------

// This is the schema of the json file
// the data attribute is an array of different elements. Arrays can be converted into different rows using explode_outer.
// the meta attribute is a nested field and can be accessed using dot notations.
dfBabyNames.printSchema()

// COMMAND ----------

// Access nested attributes using dot notations
dfBabyNames.select($"meta.view.approvals", $"meta.view.assetType", $"meta.view.attribution").show()

// COMMAND ----------

import org.apache.spark.sql.types._;

val arraySchema = StructType(
  List(
    StructField("COL01", StringType, true),
    StructField("COL02", StringType, true),
    StructField("COL03", StringType, true),
    StructField("COL04", StringType, true),
    StructField("COL05", StringType, true),
    StructField("COL06", StringType, true),
    StructField("COL07", StringType, true),
    StructField("COL08", StringType, true),
    StructField("COL09", StringType, true),
    StructField("COL10", StringType, true),
    StructField("COL11", StringType, true),
    StructField("COL12", StringType, true),
    StructField("COL13", StringType, true)
    )
);

// |[row-r9pv-p86t.ifsp, 00000000-0000-0000-0838-60C2FFCC43AE, 0, 1574264158,, 1574264158,, { }, 2007, ZOEY, KINGS, F, 11]

// Access arrays using the explode_outer function to preserve nulls as well.
import org.apache.spark.sql.functions.explode_outer;

val dfBabyNamesData = dfBabyNames
.withColumn("explodeData", explode_outer($"data"))
.drop("data", "meta");

// COMMAND ----------

dfBabyNamesData.show(5, false);

// COMMAND ----------

// MAGIC %md #### **Question #3**: Analyzing the Data
// MAGIC 
// MAGIC Using the tables you created, create a simple visualization that shows what is the most popular first letters baby names to start with in each year.

// COMMAND ----------

import org.apache.spark.sql.functions.col

val dfBabyNamesArrayCols = dfBabyNamesData.select(
  col("explodeData") +: (0 until 13).map(i => col("explodeData")(i).alias(s"col$i")): _*
)
.drop("explodeData")
.select($"col8", $"col9", $"col10")
.toDF("YEAR", "FNAME", "LNAME");

dfBabyNamesArrayCols.show(5, false);

// COMMAND ----------

import org.apache.spark.sql.functions.{substring,row_number};
import org.apache.spark.sql.expressions.Window;

val dfBabyNamesYearMostCommonLetter = dfBabyNamesArrayCols
.select(
  $"YEAR", 
  substring($"FNAME", 1, 1).alias("FIRSTNAME")
)
.groupBy($"YEAR", $"FIRSTNAME")
.count()
.withColumn("ROWNUMCOUNT", 
            row_number().over(Window.partitionBy($"YEAR").orderBy($"count".desc)))
.filter($"ROWNUMCOUNT" <= 3)
.drop("ROWNUMCOUNT");

dfBabyNamesYearMostCommonLetter.orderBy($"YEAR", $"count").show(5, false);

// COMMAND ----------

display(dfBabyNamesYearMostCommonLetter.orderBy($"YEAR", $"count"));

// COMMAND ----------

// MAGIC %md ### Log Processing
// MAGIC 
// MAGIC The following data comes from the _Learning Spark_ book.

// COMMAND ----------

// MAGIC %scala
// MAGIC display(dbutils.fs.ls("/databricks-datasets/learning-spark/data-001/fake_logs"))

// COMMAND ----------

// MAGIC %scala
// MAGIC println(dbutils.fs.head("/databricks-datasets/learning-spark/data-001/fake_logs/log1.log"))

// COMMAND ----------

// MAGIC %md #### **Question #1**: Parsing Logs
// MAGIC Parse the logs in to a DataFrame/Spark SQL table that can be queried. This should be done using the Dataset API.

// COMMAND ----------

// Read all the log files from the location using the * hint
val filePath = "/databricks-datasets/learning-spark/data-001/fake_logs/log*.log"

// Create a data frame for the logs
val dfLogs = spark.sparkContext.textFile(filePath).toDF("LOGENTRY")

// dfLogs.show(20, false)

// COMMAND ----------

import org.apache.spark.sql.functions.{split,substring, ltrim, rtrim}

// Split the log entries as per the data distrution by length or delimiters
dfLogs
.select(
  split(col("LOGENTRY"), " - - ")(0).alias("IP"),
  split(col("LOGENTRY"), " - - ")(1).alias("CONTINUE01")
)
.withColumn("DATE", substring($"CONTINUE01", 2, 26))
.withColumn("CONTINUE02", substring($"CONTINUE01", 30, 1000))
.withColumn("APICALL", split($"CONTINUE02", """"""")(1))
.withColumn("PHONE", ltrim(rtrim(split($"CONTINUE02", """"""")(2))))
.withColumn("WEBSITE", split($"CONTINUE02", """"""")(3))
.withColumn("BROWSER", split($"CONTINUE02", """"""")(5))
.drop("CONTINUE01", "CONTINUE02")
.show(false)

// COMMAND ----------

// MAGIC %md #### **Question #2**: Analysis
// MAGIC Generate some insights from the log data.

// COMMAND ----------

// The data could be regarding the API calls to different website calls.
// We can use the IP address to track the websites being accessed from it and the amount of hits resulting in failure (Ex: WEBSITE = "-").
// The date can be used to find the time distribution over IPs, Websites or Hits Sucess/Failures.
// Mozilla is the most preffered Browser used in these cases. Other browser's distribution can also be found with a larger data set.
// Same goes for Apple devices. Most hits originated from Apple devices.
// 
// Regular Expressions can also be used to find log patterns and consider only those logs which maintain a pattern using re.search.

// COMMAND ----------

// MAGIC %md
// MAGIC ### CSV Parsing
// MAGIC The following examples involove working with simple CSV data

// COMMAND ----------

// MAGIC %md #### **Question #1**: CSV Header Rows
// MAGIC Given the simple RDD `full_csv` below, write the most efficient Spark job you can to remove the header row

// COMMAND ----------

// MAGIC %scala
// MAGIC val full_csv = sc.parallelize(Array(
// MAGIC   "col_1, col_2, col_3",
// MAGIC   "1, ABC, Foo1",
// MAGIC   "2, ABCD, Foo2",
// MAGIC   "3, ABCDE, Foo3",
// MAGIC   "4, ABCDEF, Foo4",
// MAGIC   "5, DEF, Foo5",
// MAGIC   "6, DEFGHI, Foo6",
// MAGIC   "7, GHI, Foo7",
// MAGIC   "8, GHIJKL, Foo8",
// MAGIC   "9, JKLMNO, Foo9",
// MAGIC   "10, MNO, Foo10",
// MAGIC ));
// MAGIC 
// MAGIC full_csv.collect();

// COMMAND ----------

val header = full_csv.first();

val rddWithoutHeader = full_csv.filter(row => row != header);

rddWithoutHeader.collect();

// COMMAND ----------

// MAGIC %md #### **Question #2**: SparkSQL Dataframes
// MAGIC Using the `full_csv` RDD above, write code that results in a DataFrame where the schema was created programmatically based on the heard row. Create a second RDD similair to `full_csv` and uses the same function(s) you created in this step to make a Dataframe for it.

// COMMAND ----------

// Create the data frame with the column names using the header String.
val dfWithHeader = rddWithoutHeader
.map(line => line.split(","))
.map(array => (array(0), array(1), array(2)))
.toDF(header.split(","):_*);

dfWithHeader.show();

// COMMAND ----------

// MAGIC %md 
// MAGIC #### Question #3 DataFrame UDFs and DataFrame SparkSQL Functions
// MAGIC 
// MAGIC Below we've created a small DataFrame. You should use DataFrame API functions and UDFs to accomplish two tasks.
// MAGIC 
// MAGIC 1. You need to parse the State and city into two different columns.
// MAGIC 2. You need to get the number of days in between the start and end dates. You need to do this two ways.
// MAGIC   - Firstly, you should use SparkSQL functions to get this date difference.
// MAGIC   - Secondly, you should write a udf that gets the number of days between the end date and the start date.

// COMMAND ----------

// MAGIC %python
// MAGIC 
// MAGIC from pyspark.sql import functions as F
// MAGIC from pyspark.sql.types import *
// MAGIC 
// MAGIC # Build an example DataFrame dataset to work with. 
// MAGIC dbutils.fs.rm("/tmp/dataframe_sample.csv", True)
// MAGIC dbutils.fs.put("/tmp/dataframe_sample.csv", """id|end_date|start_date|location
// MAGIC 1|2015-10-14 00:00:00|2015-09-14 00:00:00|CA-SF
// MAGIC 2|2015-10-15 01:00:20|2015-08-14 00:00:00|CA-SD
// MAGIC 3|2015-10-16 02:30:00|2015-01-14 00:00:00|NY-NY
// MAGIC 4|2015-10-17 03:00:20|2015-02-14 00:00:00|NY-NY
// MAGIC 5|2015-10-18 04:30:00|2014-04-14 00:00:00|CA-SD
// MAGIC """, True)
// MAGIC 
// MAGIC formatPackage = "csv" if sc.version > '1.6' else "com.databricks.spark.csv"
// MAGIC df = sqlContext.read.format(formatPackage).options(
// MAGIC   header='true', 
// MAGIC   delimiter = '|',
// MAGIC ).load("/tmp/dataframe_sample.csv")
// MAGIC df.printSchema()

// COMMAND ----------

// MAGIC %python
// MAGIC 
// MAGIC def fnDateDiff(begin, end):
// MAGIC   return end - begin
// MAGIC 
// MAGIC from pyspark.sql.types import IntegerType, StringType
// MAGIC 
// MAGIC udfDateDiff = udf(
// MAGIC   lambda x: lower(x),
// MAGIC   StringType()
// MAGIC )
// MAGIC 
// MAGIC spark.udf.register("udf2", fnDateDiff)
// MAGIC 
// MAGIC import datetime
// MAGIC 
// MAGIC print(datetime.date.today())
// MAGIC print(datetime.date.today() + datetime.timedelta(days=10))
// MAGIC 
// MAGIC print(fnDateDiff(datetime.date.today(), datetime.date.today() + datetime.timedelta(days=10)))

// COMMAND ----------

// MAGIC %python
// MAGIC 
// MAGIC df.createOrReplaceTempView("tmpDf")
// MAGIC 
// MAGIC spark.sql("""
// MAGIC select tmpDf.*, udf2(start_date, end_date) as dt
// MAGIC from tmpDf
// MAGIC """).show()

// COMMAND ----------

// MAGIC %md
// MAGIC ### Readability
// MAGIC 
// MAGIC Write a readable, testable, and maintainable code.

// COMMAND ----------

// MAGIC %md
// MAGIC #### Question #1 SQL Readability and Performances
// MAGIC Please rewrite the 'ugly' SQL below to improve the readability and performances. Please make comment in code about how your changes inproved the SQL performance.

// COMMAND ----------

// MAGIC %md
// MAGIC select * from (
// MAGIC     select 'a' as region, 'europe' as affiliate, t1.variant_code as channel, t1.customer_key, t3.b_description as brand, t3.c_description as consumer, t3.p_category as category, t4.fmonth as fisc_month, t4.fweek as fisc_week, t1.item_timestamp as fisc_date, sum(t1.shipped_units) as shipment_units
// MAGIC     from ds_lsa.shipment_info as t1, ds_lsa.product_info as t3, ds_lsa.dayconversion as t4
// MAGIC     where t1.p_key = t3.product and try_cast(t1.item_timestamp as integer) = t4.day
// MAGIC     group by variant_code, customer_key, b_description, c_description, p_category, fmonth, fweek, item_timestamp
// MAGIC     order by variant_code, customer_key, b_description, c_description, p_category, fmonth, fweek, item_timestamp
// MAGIC )

// COMMAND ----------

// MAGIC %md
// MAGIC ###Machine Learning
// MAGIC 
// MAGIC The following examples involve using MLlib algorithms

// COMMAND ----------

// MAGIC %md #### **Question 1:** Demonstrate The Use of a MLlib Algorithm Using the DataFrame Interface(`org.apache.spark.ml`).
// MAGIC 
// MAGIC Demonstrate use of an MLlib algorithm and show an example of tuning the algorithm to improve prediction accuracy.
