-- enable dynamic partitioning

spark.sql("""
set hive.exec.dynamic.partition=true;  
set hive.exec.dynamic.partition.mode=nonstrict;  
""");

----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------

-- create hive managed table with partition on click_date

spark.sql("""
create table if not exists t_clickstream_day_session_count (
    distinct_session_count bigint 
)
partitioned by (click_date timestamp)
stored as parquet
""");

-- Get Number of sessions generated in a day.

spark.sql("""
  insert into t_clickstream_day_session_count partition(click_date)
  select
    count(distinct session_id) as distinct_session_count,
    from_unixtime(unix_timestamp(date_format(click_ts, 'YYYY-MM-dd'), 'yyyy-MM-dd')) as click_date
  from t_clickstream_sessions
  group by click_date
""");

-- Query

spark.sql("""
  select * from t_clickstream_day_session_count
""").show()

----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------

-- create hive managed table with partition on user_id clustered on click_date

spark.sql("""
create table if not exists t_clickstream_day_user_logged (
    click_date timestamp,
    time_spent_per_day_mins bigint 
)
partitioned by (user_id string)
clustered by (click_date) into 4 buckets
stored as orc
""");

-- Total time spent by a user in a day

spark.sql("""
  insert into t_clickstream_day_user_logged partition(user_id)
  select
    from_unixtime(unix_timestamp(date_format(click_ts, 'YYYY-MM-dd'), 'yyyy-MM-dd')) as click_date,
    sum(time_logged) as time_spent_per_day_mins,
    user_id
  from t_clickstream_sessions
  group by user_id, click_date
""")

-- Query

spark.sql("""
  select * from t_clickstream_day_user_logged
""").show()

----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------

-- create hive managed table with partition on user_id clustered on click_date

spark.sql("""
drop table t_clickstream_month_user_logged
""");

spark.sql("""
create table t_clickstream_month_user_logged (
    user_id string,
    time_spent_per_month_mins bigint 
)
partitioned by (click_month timestamp)
clustered by (user_id) into 4 buckets
stored as orc
""");

-- Total time spent by a user in a month

spark.sql("""
  insert into t_clickstream_month_user_logged partition(click_month)
  select
    user_id,
    sum(time_logged) as time_spent_per_month_mins,
    from_unixtime(unix_timestamp(date_format(click_ts, 'YYYY-MM'), 'yyyy-MM')) as click_month
  from t_clickstream_sessions
  group by user_id, click_month
""")

-- Query

spark.sql("""
  select * from t_clickstream_month_user_logged
""").show()

----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------