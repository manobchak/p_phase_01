-- drop and create the hive table
drop table t_clickstream;

create table if not exists t_clickstream (
    click_timestamp string,
    userid string
)
stored as textfile;

-- load data into the hive table
insert into table t_clickstream 
select '2019-01-01T11:00:00Z', 'u1'
union all
select '2019-01-01T11:15:00Z', 'u1'
union all
select '2019-01-01T12:00:00Z', 'u1'
union all
select '2019-01-01T12:20:00Z', 'u1'
union all
select '2019-01-01T15:00:00Z', 'u1'
union all
select '2019-01-01T11:00:00Z', 'u2'
union all
select '2019-01-02T11:00:00Z', 'u2'
union all
select '2019-01-02T11:25:00Z', 'u2'
union all
select '2019-01-02T11:50:00Z', 'u2'
union all
select '2019-01-02T12:15:00Z', 'u2'
union all
select '2019-01-02T12:40:00Z', 'u2'
union all
select '2019-01-02T13:05:00Z', 'u2'
union all
select '2019-01-02T13:20:00Z', 'u2'
union all
select '2019-01-02T13:51:00Z', 'u2'
union all
select '2019-01-02T14:20:00Z', 'u2'
union all
select '2019-01-02T14:30:00Z', 'u2';