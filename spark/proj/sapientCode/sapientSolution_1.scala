// declaring data types
case class Person(name: String, age: Integer, location: String);

// setting up data
val p1 = Person("Rajesh", 21, "London");
val p2 = Person("Suresh", 28, "California");
val p3 = Person("Sam", 26, "Delhi");
val p4 = Person("Rajesh", 21, "Gurgaon");
val p5 = Person("Manish", 29, "Bengaluru");

// list of persons
val Persons = List(p1, p2, p3, p4, p5)

// distinct name and age
val DistinctPersons = Persons.groupBy(record => (record.name.trim(), record.age)).map(_._2.head).toList

// display the name and age only
DistinctPersons.foreach(tup => println(tup.name, tup.age))