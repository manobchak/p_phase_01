// Intial Data:
// +--------------------+-------+
// |        click_ts_str|user_id|
// +--------------------+-------+
// |2019-01-01T11:00:00Z|     u1|
// |2019-01-01T11:15:00Z|     u1|
// |2019-01-01T12:00:00Z|     u1|
// |2019-01-01T12:20:00Z|     u1|
// |2019-01-01T15:00:00Z|     u1|
// |2019-01-01T11:00:00Z|     u2|
// |2019-01-02T11:00:00Z|     u2|
// |2019-01-02T11:25:00Z|     u2|
// |2019-01-02T11:50:00Z|     u2|
// |2019-01-02T12:15:00Z|     u2|
// |2019-01-02T12:40:00Z|     u2|
// |2019-01-02T13:05:00Z|     u2|
// |2019-01-02T13:20:00Z|     u2|
// |2019-01-02T13:51:00Z|     u2|
// |2019-01-02T14:20:00Z|     u2|
// |2019-01-02T14:30:00Z|     u2|
// +--------------------+-------+
// 
// Final Data: session_id is a hash value.
// +-------+-----------+-------------------+-----------+-------------------------+
// |user_id| session_id|           click_ts|time_logged|last_session_reset_reason|
// +-------+-----------+-------------------+-----------+-------------------------+
// |     u1|-2037009156|2019-01-01 11:00:00|        0.0|              First_Login|
// |     u1|-2037009156|2019-01-01 11:15:00|       15.0|                   Active|
// |     u1| -155252917|2019-01-01 12:00:00|        0.0|               Inactivity|
// |     u1| -155252917|2019-01-01 12:20:00|       20.0|                   Active|
// |     u1|  736547093|2019-01-01 15:00:00|        0.0|               Inactivity|
// |     u2|  674477711|2019-01-01 11:00:00|        0.0|              First_Login|
// |     u2|  295760391|2019-01-02 11:00:00|        0.0|               Inactivity|
// |     u2|  295760391|2019-01-02 11:25:00|       25.0|                   Active|
// |     u2|  295760391|2019-01-02 11:50:00|       25.0|                   Active|
// |     u2|  295760391|2019-01-02 12:15:00|       25.0|                   Active|
// |     u2|  295760391|2019-01-02 12:40:00|       25.0|                   Active|
// |     u2|  581424045|2019-01-02 13:05:00|       25.0|          Session_Timeout|
// |     u2|  581424045|2019-01-02 13:20:00|       15.0|                   Active|
// |     u2| -638275015|2019-01-02 13:51:00|        0.0|               Inactivity|
// |     u2| -638275015|2019-01-02 14:20:00|       29.0|                   Active|
// |     u2| -638275015|2019-01-02 14:30:00|       10.0|                   Active|
// +-------+-----------+-------------------+-----------+-------------------------+

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// import the required libraries
import org.apache.spark.sql.expressions.Window;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.functions.{unix_timestamp, lit, col, lag, row_number, sum, when, hash, concat};

// function to generate the session_id for clicks without a session
def fnGenerateSessionId (dfNullSession: DataFrame): DataFrame = {

  // generate new session_id for timeout due to 30 mins activity
  val vWindowSpec = Window.partitionBy("user_id").orderBy("click_ts");

  val dfStg01Tmo30mins = dfNullSession
  .select(
    $"user_id",
    $"click_ts",
    lag($"click_ts", 1).over(vWindowSpec).alias("prev_click_ts"))
  .withColumn(
    "time_diff_mins", (col("click_ts").cast("long") - col("prev_click_ts").cast("long"))/60
  )
  .withColumn(
    "session_id", 
    when(
      row_number.over(vWindowSpec) === 1 || $"time_diff_mins" >= 30, hash(concat($"user_id", $"click_ts"))
    ).otherwise(-1)
  );

  // find the start of each session
  val dfStg02SessionId = dfStg01Tmo30mins.filter($"session_id" =!= -1);
  
  // take the session just before the click
  val dfStg03Sessions = dfStg01Tmo30mins.alias("dfAll")
  .join(
    dfStg02SessionId.alias("dfSess"),
    $"dfAll.user_id" === $"dfSess.user_id"
    && $"dfAll.click_ts" > $"dfSess.click_ts"
    && ((($"dfAll.click_ts".cast("long") - $"dfSess.click_ts".cast("long"))/60)) < 120,
    "left"
  )
  .withColumn(
    "time_diff_session_start", ($"dfAll.click_ts".cast("long") - $"dfSess.click_ts".cast("long"))/60
  )
  .withColumn(
    "rn", row_number().over(Window.partitionBy($"dfAll.user_id", $"dfAll.click_ts").orderBy($"time_diff_session_start"))
  )
  .filter($"rn" === 1)
  .select($"dfAll.*", $"dfSess.session_id".alias("session_id_start"), $"dfSess.click_ts".alias("session_click_ts"), $"time_diff_session_start", $"rn");
  
  // take the session just before the click
  val dfStg04Fin = dfStg03Sessions
  .withColumn(
    "session_id_fin", 
    when($"session_id" === -1, $"session_id_start").otherwise($"session_id")
  )
  .select($"user_id", $"click_ts", $"session_id_fin".alias("session_id"));
  
  dfStg04Fin;

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// load the data from hive table to a df
val dfClickstream01Src = spark.table("t_clickstream").toDF("click_ts_str", "user_id");

// convert the click string to timestamp
val vTsFormat = unix_timestamp($"click_ts_str", "yyyy-MM-dd'T'HH:mm:ss'Z'").cast("timestamp");

:paste
val dfClickstream02Ts = 
dfClickstream01Src
.withColumn("click_ts", vTsFormat)
.withColumn("session_id", lit(null).cast("long"))
.select($"click_ts", $"user_id", $"session_id");

spark.catalog.refreshTable("t_clickstream_stg_holding");
dfClickstream02Ts.write.mode("overwrite").saveAsTable("t_clickstream_stg_holding");

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// the while loop will iterate for sessions which been going on for more than 2 hours
// sessions active for more than 2 hours will be given null session_id by the function
// these clicks will be resent to the function for generating their session ids
// hive tables since the df can not be accessed outside the while loop

var vCnt = 1L;

do {
  // get all the clicks without session_id
  val dfHolding = spark.table("t_clickstream_stg_holding").filter($"session_id".isNull);

  // the functions returns a dataframe with session ids populated.
  // null will be populated for clicks with session active for more than 2 hours.
  val dfReturn = fnGenerateSessionId(dfHolding).select($"click_ts", $"user_id", $"session_id").persist();

  // create a temp holding table for the current data in the hive table
  val dfClickStreamHive = spark.table("t_clickstream_stg_holding");

  // join the dataframe returned from the function with the hive dataset
  // get the clicks with session ids populated
  // this helps in the iteration when sending clicks with session ids as null (due to session > 2 hours)
  val dfClickStreamFin = dfClickStreamHive
  .union(dfReturn)
  .withColumn(
    "rn",
    row_number().over(Window.partitionBy($"user_id", $"click_ts").orderBy($"session_id".desc))
  )
  .filter($"rn" === 1)
  .select($"click_ts", $"user_id", $"session_id");

  // refresh the hive tables
  // reinsert the data into the holding table. this will act as the source for the next iteration.
  // the clicks with sessions active for more than 2 hours will be resent in the next iteration
  spark.catalog.refreshTable("t_clickstream_stg_holding");
  dfClickStreamFin.write.mode("overwrite").saveAsTable("t_clickstream_stg_return");
  spark.catalog.refreshTable("t_clickstream_stg_return");
  spark.table("t_clickstream_stg_return").write.mode("overwrite").insertInto("t_clickstream_stg_holding");
  
  // if all the clicks have been assigned a session_id exit the loop
  vCnt = spark.table("t_clickstream_stg_holding").filter($"session_id".isNull).count
  println("--> vCnt : ", vCnt)
} while (vCnt > 0)

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// fetch the final data from the holding (stage) table
val dfClickstreamHolding = spark.table("t_clickstream_stg_holding");

// populate the following columns
// time_logged: how long the last click was active (reset in case of new sessions and continued in case of sessions > 2 hours)
// last_session_reset_reason: values = {"Session_Timeout", "Inactivity", "First_Login", "Active"}
// Session_Timeout: when session was active for more than 2 hours
// Inactivity: no clicks for 30 mins
// First_Login: first click by the user
// Active: Active click
:paste
val dfClickstreamSession = dfClickstreamHolding
.select(
  $"user_id",
  $"click_ts",
  $"session_id"
)
.withColumn("prev_click_ts", lag($"click_ts", 1).over(Window.partitionBy($"user_id").orderBy($"click_ts")).alias("prev_click_ts"))
.withColumn("time_diff_mins", (col("click_ts").cast("long") - col("prev_click_ts").cast("long"))/60)
.withColumn("rn", row_number().over(Window.partitionBy($"session_id").orderBy($"click_ts")))
.withColumn("time_logged", when($"rn" === 1 && ($"time_diff_mins" > 30 || $"time_diff_mins".isNull), 0)otherwise($"time_diff_mins"))
.withColumn(
  "last_session_reset_reason",
  when($"rn" === 1 && $"time_diff_mins" < 30, "Session_Timeout")
  .when($"rn" === 1 && $"time_diff_mins".isNull, "First_Login")
  .when($"rn" === 1, "Inactivity")
  .otherwise("Active")
)
.select($"user_id", $"session_id", $"click_ts", $"time_logged", $"last_session_reset_reason")
.orderBy($"user_id", $"click_ts")
.persist();

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// store the file as a parquet file partitioned by click_ts
// click_ts since most of the data operations will be done on this column
val vFileLoc = "/user/manavchak1039/opParquet/clickstreamSessionData";
dfClickstreamSession.write.partitionBy("click_ts").mode("overwrite").format("parquet").save("vFileLoc");

// save the df as a hive table
// partitioned on click_ts
// bucketed by user_id and sorted on session_id
val vNumOfBuckets = 4;
dfClickstreamSession.write.partitionBy("click_ts").bucketBy(vNumOfBuckets, "user_id").sortBy("session_id").mode("overwrite").saveAsTable("t_clickstream_sessions");

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

