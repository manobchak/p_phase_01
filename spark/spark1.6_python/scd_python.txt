hadoop fs -put -f EMPLOYEE_MAIN1.csv /user/stringmad/POC/SCD/EMPLOYEE_MAIN/
hdfs dfs -cat /user/stringmad/POC/SCD/EMPLOYEE_MAIN/EMPLOYEE_MAIN1.csv

hdfs dfs -put -f EMPLOYEE_STG1.csv /user/stringmad/POC/SCD/EMPLOYEE_STG/
hdfs dfs -cat /user/stringmad/POC/SCD/EMPLOYEE_STG/EMPLOYEE_STG1.csv

-- stg1: import from incremental feed file
from pyspark.sql.types import *

schema_df_stg1 = StructType([
    StructField("EMP_ID", IntegerType()),
    StructField("EMP_NAME", StringType()),
    StructField("PHONE_NO", IntegerType())
])

df_stg1 = spark.read.csv('/user/stringmad/POC/SCD/EMPLOYEE_STG/EMPLOYEE_STG1.csv', header=True, schema=schema_df_stg1)

-- tgt: load target table
df_tgt_temp = spark.read.csv('/user/stringmad/POC/SCD/EMPLOYEE_MAIN').toDF('EMP_ID','EMP_NAME','PHONE_NO','ED_START_DATE','ED_END_DATE')

df_tgt = df_tgt_temp.select(df_tgt_temp.EMP_ID.cast("int"),df_tgt_temp.EMP_NAME,df_tgt_temp.PHONE_NO.cast("int"),df_tgt_temp.ED_START_DATE.cast("timestamp"),df_tgt_temp.ED_END_DATE.cast("timestamp"))

-- stg2

from pyspark.sql import functions as F
df_stg2 = df_stg1.select('*', F.current_timestamp().alias('ED_START_DATE'), F.lit(None).alias('ED_END_DATE'))

-- tgt: incremental

df_stg_incr = df_tgt.join(df_stg2, on=['EMP_ID'], how='inner').select(df_tgt.EMP_ID, df_tgt.EMP_NAME, df_tgt.PHONE_NO, df_tgt.ED_START_DATE, df_stg2.ED_START_DATE.alias('ED_END_DATE'))
df_stg_incr.show(20, False)

-- tgt: not incremental
df_stg_no_changes = df_tgt.join(df_stg2, on=['EMP_ID'], how='left_anti')
df_stg_no_changes.show(20, False)

-- union

df_stg_fin = df_stg2.union(df_stg_incr).union(df_stg_no_changes)
df_stg_fin.sort('EMP_ID', 'ED_END_DATE', ascending=[False,True]).show(20, False)

---------------------------------------------------------------------------------------------------------------------------------------

# Creating a spark session

from pyspark.sql import SparkSession

spark = SparkSession \
    .builder \
    .appName("SCD_Test_Run") \
    .getOrCreate()
    
# Code

from pyspark.sql.types import *

schema_df_stg1 = StructType([
    StructField("EMP_ID", IntegerType()),
    StructField("EMP_NAME", StringType()),
    StructField("PHONE_NO", IntegerType())
])

df_stg1 = spark.read.csv('/user/stringmad/POC/SCD/EMPLOYEE_STG/EMPLOYEE_STG1.csv', header=True, schema=schema_df_stg1)

df_stg2 = df_stg1.filter(df_stg1.EMP_ID > 500)

df_stg2.write.csv('/user/stringmad/POC/SCD/EMPLOYEE_STG/DATA_EXPORT')

----

hdfs dfs -ls /user/stringmad/POC/SCD/EMPLOYEE_STG/DATA_EXPORT
hdfs dfs -rmr /user/stringmad/POC/SCD/EMPLOYEE_STG/DATA_EXPORT

spark2-submit \
--master yarn \
--deploy-mode cluster \
--executor-cores=2 \
--executor-memory 1g \
--num-executors 4 \
/home/stringmad/pyspark_scd/bin/script_test_run.py

---------------------------------------------------------------------------------------------------------------------------------------


-- subtract 1ms
df_tgt.select('*', F.date_sub(F.col("ED_START_DATE"), 1/3600).cast('timestamp').alias('PREV_END_DATE')).show(20, False)

df_tgt.createOrReplaceTempView('t_df_tgt')
df_tgt_n1 = spark.sql("select a.*, (ED_START_DATE - INTERVAL 1 MILLISECOND) as NEW_ED_END_DATE from t_df_tgt a").show(20, False)


86400000
-- testing

schema_df_tgt = StructType([
    StructField("EMP_ID", IntegerType()),
    StructField("EMP_NAME", StringType()),
    StructField("PHONE_NO", IntegerType()),
    StructField("ED_START_DATE", TimestampType()),
    StructField("ED_END_DATE", TimestampType())
])

df_tgt2 = spark.read.csv('/user/stringmad/POC/SCD/EMPLOYEE_MAIN/EMPLOYEE_MAIN1.csv', header=True, schema=schema_df_tgt, timestampFormat = "yyyy-MM-dd HH:mm:ss")

111,Jhoney,1234567890,2016-06-01 08:16:41.0,NULL 

csvSchema = StructType([ 
  StructField("Timestamp",TimestampType(),True), 
  StructField("Name",StringType(),True), 
  StructField("Value",DoubleType(),True)
])

df = spark.read \ 
.csv(file_path, header = True, multiLine = True, escape = "\"", schema = csvSchema, timestampFormat = "MM/dd/yyyy HH:mm:ss.SSSZZ" ) \
.withColumn("year", date_format(col("Timestamp"), "yyyy").cast(IntegerType())) \ 
.withColumn("month", date_format(col("Timestamp"), "MM").cast(IntegerType()))

val eventDataDF = spark.read
                             .option("header", "true")
                             .option("inferSchema","true")
                             .option("timestampFormat", "MM-dd-yyyy hh mm ss")
                             .csv("d://spark-example/event_data1.csv");