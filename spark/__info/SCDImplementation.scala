object SCDImplementation extends java.io.Serializable {

def SCD2(new_table: DataFrame, old_table: DataFrame, old_primary_key: String, listOfColumnsToRejectCompare: String*): (DataFrame, DataFrame) = {
    val sqlContext = old_table.sqlContext;
    val sc = sqlContext.sparkContext;

    val sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    val now = new Date();
    val yesterday = new Date(System.currentTimeMillis() - 24 * 60 * 60 * 1000);
    val scdColumns = Array("ed_start_date", "ed_ef_end_date");
	
    //md5 function to generate hash value of each column - for comparison
    def md5Func(s: Any) = {
      if (s == null) {
        new String("");
      } else {
        new String(java.security.MessageDigest.getInstance("MD5").digest(s.toString.getBytes));
      }
    };
    
    //register md5 function
    val md5 = org.apache.spark.sql.functions.udf(md5Func _);

    //Fetch old table schema without ed_start_date,ed_end_date
    val oldTableSchema = old_table.columns.filter(x => !scdColumns.contains(x))
    
    //create DF with above schema 
    val new_table_renamed = new_table.distinct.toDF(oldTableSchema:_*);

    //fetch the column-names to be compared for updation
    var hashCompareSchema: Array[Column] = null;
    if (listOfColumnsToRejectCompare == null) {
      hashCompareSchema = oldTableSchema.map(c => col(c))
    } else {
      hashCompareSchema = oldTableSchema.filter { x => !listOfColumnsToRejectCompare.contains(x) }.map(c => col(c))
    }
    
    //new records with primary_key and hash value of remaining columns
    val new_hash = new_table_renamed.na.fill("null").select(col(old_primary_key), concat(hashCompareSchema: _*).alias("___merged")).withColumn("hash_value", md5(col("___merged")).as("hash_value")).select(col(old_primary_key), col("hash_value")).persist(StorageLevel.MEMORY_AND_DISK);
  
  
    //old records with primary_key and hash value of remaining columns
    val old_hash = old_table.na.fill("null").select(col(old_primary_key), concat(hashCompareSchema: _*).alias("___merged")).withColumn("hash_value", md5(col("___merged")).as("hash_value")).select(col(old_primary_key), col("hash_value")).persist(StorageLevel.MEMORY_AND_DISK);
 
    
    //store primary keys whose record got updated
    val hash_diff = new_hash.except(old_hash).select(old_primary_key).persist();


    //store primary keys whose records are not updated
    val old_IDs = old_table.select(old_primary_key).distinct.except(hash_diff)


     //old records which are not updated
    val old_valid_records = old_table.distinct.join(old_IDs, Seq(old_primary_key)).select(old_table.columns.map(old_table(_)): _*).distinct();

 
    val fresh_records_ids = hash_diff.except(old_valid_records.select(old_primary_key)).distinct();


    //distinct new record's primary key
    val new_records_table = new_table_renamed.join(fresh_records_ids, Seq(old_primary_key)).select(new_table_renamed.columns.map(new_table_renamed(_)): _*).withColumn("ed_start_date", lit(sdf.format(now))).withColumn("ed_update_date", lit("")).distinct().select(old_table.columns.map(x => new Column(x)): _*);


    val new_records_ids = hash_diff.except(old_table.select(old_primary_key)).distinct();


    val update_record_ids = new_hash.select(old_primary_key).except(old_IDs).except(new_records_ids).distinct.persist();

 
val updated_records_All = old_table.distinct.alias("ot").join(update_record_ids, Seq(old_primary_key)).join(new_table_renamed, Seq(old_primary_key)).select("ot.*").select(old_table.columns.map(x => new Column(x)): _*).distinct.persist()


    val updated_records_withChange = updated_records_All.filter(col("ed_update_date").===(""))

   
    val updated_records_withoutChange = updated_records_All.filter(col("ed_update_date").!==(""))
 
    
    val updated_records_changed = updated_records_withChange.drop("ed_update_date").withColumn("ed_update_date", lit(sdf.format(yesterday)))

    updated_records_All.unpersist();
    
    
    val staging_table = new_records_table.unionAll(updated_records_changed).unionAll(updated_records_withoutChange).distinct.persist()
    
    hash_diff.unpersist
    new_records_ids.unpersist
    update_record_ids.unpersist

    return (old_valid_records.unionAll(staging_table), staging_table);
  }

}