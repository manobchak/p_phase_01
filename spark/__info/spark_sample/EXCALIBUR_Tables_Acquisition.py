from pyspark.sql import HiveContext
from pyspark import SparkConf, SparkContext, StorageLevel
from pyspark.sql import SQLContext
from pyspark.sql.types import *
from pyspark.sql.functions import *
from pyspark.sql.window import Window
from pyspark.sql import Row
from pyspark.sql.functions import lit
import sys
from datetime import date

conf = (SparkConf())

sc = SparkContext(conf=conf)
sqlContext = HiveContext(sc)

src_instance=sys.argv[1]
cols_list=sys.argv[2]
sel_cols_list=sys.argv[3]
src_table=sys.argv[4]
src_table_del=sys.argv[5]
trg_location=sys.argv[6]
trg_instance=sys.argv[7]
trg_table=sys.argv[8]

def load_table_daily(fn_src_instance,fn_cols_list,fn_sel_cols_list,fn_src_table,fn_src_table_del,fn_trg_location,fn_trg_instance,fn_trg_table):
	d = date.today()
	fn_partition = str(d.year) + d.strftime('%m') + str(d.day)
	var_src_query = "SELECT RUN_DATE,"+ fn_cols_list + ",RECORD_TYPE FROM " + fn_src_instance + "." + fn_src_table
	var_sel_cols = fn_sel_cols_list.split(",")
	src_tab_df = sqlContext.sql(var_src_query)
	src_tab_df_stg1 = src_tab_df.filter(src_tab_df.RUN_DATE == fn_partition).filter("RECORD_TYPE IN ('I','U')").select(var_sel_cols).withColumn("DELETED_FLAG",lit('N'))
	var_src_del_query = "SELECT RUN_DATE,"+ fn_cols_list + " FROM " + fn_src_instance + "." + fn_src_table_del	
	src_tab_del_df = sqlContext.sql(var_src_del_query)
	src_tab_del_df_stg1 = src_tab_del_df.filter(src_tab_del_df.RUN_DATE == fn_partition).select(var_sel_cols).withColumn("DELETED_FLAG",lit('Y'))
	trg_tab_df = src_tab_df_stg1.unionAll(src_tab_del_df_stg1).coalesce(5)
	trg_tab_df.registerTempTable("trg_tab")
	trg_query = "INSERT OVERWRITE TABLE "+ trg_instance + "." + trg_table + " SELECT " + fn_cols_list + ",DELETED_FLAG FROM trg_tab"
	sqlContext.sql(trg_query)
	
if __name__ == '__main__':
	load_table_daily(src_instance,cols_list,sel_cols_list,src_table,src_table_del,trg_location,trg_instance,trg_table)
	