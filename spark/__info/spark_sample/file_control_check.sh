#*************************************************
#file_SF_opportunity_hold_control_check.sh
#*************************************************

#!/bin/bash -e

#login to functional account

kinit -kt EDWB2BP1.keytab EDWB2BP1@IUSER.IROOT.ADIDOM.COM    

trg_tab=$1

cnt_chk = `beeline --showHeader=false --outputformat=csv2 --hiveconf mapreduce.job.queuename=NONP.HAASAAP0082_06429 -u 'jdbc:hive2://tplhc01c002:10000/'"HAASAAP0082_06429"';principal=hive/tplhc01c002.iuser.iroot.adidom.com@IUSER.IROOT.ADIDOM.COM' -e "SELECT COUNT(*) FROM HAASAAP0748_13626.EXCALIBUR_AUDIT WHERE FEED_TYPE = '${trg_tab}' AND RUN_DATE = TO_DATE(CURRENT_TIMESTAMP) AND UPPER(STATUS) = 'SUCCESS'"`

if [ cnt_chk -gt 0 ];
then
  echo "FeedArrived=true"
else
  echo "FeedArrived=false"
fi

